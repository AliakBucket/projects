#include <stdio.h>
#include <stdlib.h>
#include <time.h> //we use this library for time()
#include <stdbool.h>// we use this library for boolean variables

//multiple functions help us make re-usable code
int binarySearch(int *, int , int );
void printer(int *,int);
void swap(int *, int *);
void initializeArray(int *, int);
void SortInt(int *, int);
void AddSortInt(int *,int, int);
void RemSort(int *,int, int);

int main(){

int nElem, newV, *ArrayInt;

printf("Please give me the size of the array: ");
scanf("%d",&nElem);
if ( !(ArrayInt = (int*) malloc(nElem*sizeof(int))) ){
    // we check if malloc fails to show a message end exit
    printf("You are out of memory!\nPlease contact with your Administrator.\n");
    exit(-1);
}
initializeArray(ArrayInt, nElem);
SortInt(ArrayInt,nElem);
printf("We sort the list as follows:\n");
printer(ArrayInt, nElem);

printf("Add a number in your list: ");
scanf("%d",&newV);
nElem++;
if ( !(ArrayInt = (int*) realloc(ArrayInt, (nElem)*sizeof(int))) ){//changes the size of ArrayInt
    // we check if malloc fails to show a message end exit
    printf("You are out of memory!\nPlease contact with your Administrator.\n");
    exit(-1);
}
AddSortInt(ArrayInt, newV, nElem);
printf("\nWe Added the new number in the sorted list.\nThe new list is:\n");
printer(ArrayInt, nElem);

printf("Give me the Value you want to remove from the list: ");
scanf("%d",&newV);
RemSort(ArrayInt,newV,nElem);
nElem--;
if ( !(ArrayInt = (int*) realloc(ArrayInt, (nElem)*sizeof(int))) ){//changes the size of ArrayInt
    // we check if malloc fails to show a message end exit
    printf("You are out of memory!\nPlease contact with your Administrator.\n");
    exit(-1);
}
printf("\nWe removed the number you gave us from the sorted list.\nThe new list is:\n");
printer(ArrayInt, nElem);


free(ArrayInt);// we always free the memory that we have allocate, at the end of the program
return 0;
}
//we use swap function to make the bubble sort
void swap(int *x, int *y){
    int temp = *x;//we are using a temp int to help us with swap
    *x = *y;
    *y = temp;}
// this function prints the elements of an array
void printer(int arr[], int n){
    int i;
    i=0;
    while(i<n){
        printf("%i ", arr[i]);
        i++;
    }
    printf("\n");
}
// we fill the array with random integers
void initializeArray(int arr[], int n){
    int i;
    srand((unsigned)time(NULL));//initialize the range of random number, only once
    for (i=0; i<n; i++){
        arr[i]= rand() % 101; //the rand returns a random number from the srand and it is scaling between [0,100]
    }
}
//we sort the array using bubble sort
void SortInt(int * arr,int n){
   int i, j;
   bool swapped;
   for (i = 0; i < n-1; i++){
    swapped=false;
// when this for loop completes the smallest integer is on the top
       for (j = 0; j < n-i-1; j++){//so the next for loop must be by one element shorter
           if (arr[j] > arr[j+1]){
              swap(&arr[j], &arr[j+1]);
              swapped=true;
            }
       }
// IF nothing change then exit from the loop
     if (swapped == false){break;}
   }
}
//Adds the new value to the array
void AddSortInt(int arr[],int newV, int n){

 int * end;
 end = arr + n -1;// points the end of the array
 *end = newV;//save the new value
 SortInt(arr,n);// we sort again with the new value
}
//we use the binary method to find the position of the value
int binarySearch(int arr[], int siz, int x){
    int start, end , middle;
    start=0;
    end = siz-1;
    while(start <=end){
        middle= (start+end)/2;
        if (x< arr[middle]){
            end = middle-1;
        }else if (x> arr[middle]){
            start = middle+1;
        }else{
            return middle;
        }
    }
    return -1;
}
//This function removes the given value by swapping to the bottom
void RemSort(int * arr,int remV, int siz){
    int pos;
    pos = binarySearch(arr,siz, remV);
    if (pos<0){
        printf("The number you have entered is not on the list.\nSo we will delete the last one...");
        return;
    }else{
        //we have the position of the key element
        //now we will swap it to the bottom
        int next = pos+1;
        while(pos<siz) {//swaps the array from the position of the given value to the end of the list
            swap(&arr[pos],&arr[next]);
            pos++;
            next++;
        }
        arr[pos]=NULL;//we make it NULL
    }
}
