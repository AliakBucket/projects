#include <stdio.h>
#include <stdlib.h>
#include <string.h>//we need this library for functions that manage strings

#define m 100  // converts the "m" in to 100
void reverse(char *);
void addhash(char *);
void revadd(char *);

int main() {

char word[m], temp[m];

printf("Please enter a word to revert: ");
fgets(word,m,stdin);
strcpy(temp,word);
reverse(temp);
printf("This is the reverse: %s\n", temp);
printf("\nPlease enter a word to modify: ");
fgets(word,m/2, stdin);// the user has a limit because we will add characters on the array later
strcpy(temp,word);
addhash(temp);
printf("This is the word with hashtags : %s", temp);
printf("\nPlease enter a word to revert and modify: ");
fgets(word,m/2, stdin);
strcpy(temp,word);
revadd(temp);
printf("This is the reversed and modified word: %s\n",temp);

return 0;}

void reverse(char * wrd){
    strrev(wrd); // we call a function from string.h library that reverses the string
}

void addhash(char * wrd){
    char temp[m], *begin, *end, *add;// we use two pointers, one on the start and one on the end of the array
    begin = wrd;// we initialize the pointers, we don't want to loose the original pointer
    end = wrd;
    add = temp;
    int i,len;
    i=0;
    len = strlen(wrd);// the length with the ending character \0
    end += len -1;//points the end of the array
    for (i=0; i<len-1; i++){// we fill the temp array
        *add=*begin;
        begin++;
        add++;
        if (*begin!=*end){
            *add='#';
            add++;
        }else{
            i=len;
            *add='\0';
        }
    }//here we copy the array we made on the original
    strcpy(wrd, temp);
}
// we use the previous functions to create an reversed with hashtags string
void revadd(char * wrd){
    addhash(wrd);
    reverse(wrd);
}
